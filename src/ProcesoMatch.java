import java.util.ArrayList;

public class ProcesoMatch {

	public void procesoMatch(int[] a, int[] b){
		int i = 0;
		int j = 0;
		
		ArrayList<Integer> match = new ArrayList<Integer>();
		ArrayList<Integer> maestroSinDetalle = new ArrayList<Integer>();
		ArrayList<Integer> detalleSinMaestro = new ArrayList<Integer>();
		
		
		while ( i < a.length && j < b.length ) {
			if ( a[i] == b[j] ) {
				//Match
				match.add(a[i]);
				i++;
				j++;
			} else if ( a[i] > b[j] ){
				//Detalle sin Maestro
				detalleSinMaestro.add(b[j]);
				j++;
			} else {
				//Maestreo sin Detalle
				maestroSinDetalle.add(a[i]);
				i++;
			}
		}
		
		while ( i < a.length ) {
			// Maestro sin Detalle
			maestroSinDetalle.add(a[i]);
			i++;
		}
		
		while ( j < b.length ) {
			// Detalle sin Maestro
			detalleSinMaestro.add(b[j]);
			j++;
		}
		
		System.out.println("Match en el array: " + match );
		System.out.println("Maestro sin detalle: " + maestroSinDetalle );
		System.out.println("Detalle sin maestro: " + detalleSinMaestro );
		
	}
	
}
